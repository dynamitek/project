import React, { PureComponent } from 'react';
import {connect} from 'react-redux'

import {Day} from '../../components/Day';
import * as postsSelectors from '../../reducers/posts';
import * as statsSelectors from '../../reducers/stats';
import {ONE_DAY_MS} from '../../constants/time';

const DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

const getTitle = date => {
    const nowDay = (new Date(Date.now())).getDate();
    const month = MONTHS[(new Date(date)).getMonth()];
    const day = (new Date(date)).getDate();

    if (nowDay === day) {
        return 'Today';
    }

    return `${day} ${month}`;
};

const getDescription = date => {
    return DAYS[(new Date(date)).getDay()];
};

const mapStateToProps = (state, props) => ({
    title: getTitle(props.date),
    description: getDescription(props.date),
    cards: postsSelectors.getPostsInRange(state, props.date, props.date + ONE_DAY_MS),
    counters: statsSelectors.getDayCounters(state, props.date)
});

export class ActualDayContainer extends PureComponent {
    render() {
        const {title, description, cards, counters} = this.props;

        return (
            <Day
                title={title}
                description={description}
                cards={cards}
                counters={counters}
            />
        );
    }
}

export const ActualDay = connect(
    mapStateToProps
)(ActualDayContainer);