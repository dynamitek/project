import React, { PureComponent } from 'react';

import {Days} from '../../components/Days';
import {ActualDay} from '../ActualDay';
import {ONE_DAY_MS} from '../../constants/time';

const now = new Date(Date.now());
now.setHours(0, 0, 0);
const nowDay = now.getTime();
const old = (new Date(nowDay - ONE_DAY_MS * 14)).getTime();

export class ActualDays extends PureComponent {
    render() {
        const arr = [...Array(14)];

        return (
            <Days>
                {arr.map((day, index) => {
                    const date = old + (ONE_DAY_MS * (index + 1));

                    return (
                        <ActualDay key={date} date={date} />
                    );
                })}
            </Days>
        );
    }
}
