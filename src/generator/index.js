const fs = require('fs');

const getRandomInt = (min, max) =>
    Math.floor(Math.random() * (max - min)) + min;

const accounts = [
    {
        id: 1,
        social: 'instagram',
        username: 'dynamitek',
        avatar: 'https://pp.userapi.com/c623730/v623730395/389e0/WBqyu2UJe2c.jpg'
    },
    {
        id: 2,
        social: 'facebook',
        username: 'sferamail',
        avatar: 'https://pp.userapi.com/c417829/v417829395/e04/Jr3nn2DVol8.jpg'
    },
    {
        id: 3,
        social: 'twitter',
        username: 'sferatwit',
        avatar: 'https://pp.userapi.com/c840625/v840625904/98eb/5B22JOl3yeM.jpg'
    },
    {
        id: 4,
        social: 'youtube',
        username: 'ivanov-vadim',
        avatar: 'https://pp.userapi.com/c417829/v417829395/e04/Jr3nn2DVol8.jpg'
    },
    {
        id: 5,
        social: 'youtube',
        username: 'user-v',
        avatar: 'https://pp.userapi.com/c841320/v841320077/7caa2/VWHd5Ea3NG0.jpg'
    },
    {
        id: 6,
        social: 'googlePlus',
        username: 'google-ivanov',
        avatar: 'https://pp.userapi.com/c840225/v840225204/78909/nbsp1gCZRtU.jpg'
    }
];

const genText = () => {
    const texts = [
        '"We’re mistaken when we think that technology just automatically improves...it only improves if a lot of people work very hard to make it better." — Elon Musk, #TED2017 Catch Elon\'s full TED watch"',
        '"We’re mistaken when we think that technology just automatically improves... Elon Musk, #TED2017 Catch Elon\'s full TED watch"',
        'We’re mistaken when we think that technology just automatically improves...',
        'We’re mistaken when we improves...',
        'Elon Musk, #TED2017 Catch Elon\'s full TED watch',
        'Lorem ipsum dolor sit amet, consectetur'
    ];

    return texts[getRandomInt(0, texts.length)];
};

const genDate = (start, end) => {
    const diff = end.getTime() - start.getTime();
    const newDiff = diff * Math.random();

    return (new Date(start.getTime() + newDiff)).getTime();
};

const genPosts = (quantity, {start, end}) =>
    [...Array(quantity)].map((item, index) => ({
        id: index,
        text: genText(),
        pubDate: genDate(start, end)
    }));

const genStats = (posts, accounts) =>
    posts.reduce((acc, post) => {
        const stats = [...Array(getRandomInt(1, 4))].map(() => ({
            postId: post.id,
            accountId: getRandomInt(1, accounts.length),
            likes: getRandomInt(1, 200),
            retweets: getRandomInt(1, 100),
            comments: getRandomInt(1, 80),
            views: getRandomInt(1, 700)
        }));

    return [...acc, ...stats];
}, []);

const genState = (postsQuantity, dateRange) => {
    const posts = genPosts(postsQuantity, dateRange);
    const stats = genStats(posts, accounts);

    return {
        accounts,
        posts,
        stats
    };
};

const state = genState(200, {
    start: new Date(2018, 1, 15),
    end: new Date()
});

const stingState = JSON.stringify(state, null, 2);

fs.writeFile('state.json', stingState, (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
});