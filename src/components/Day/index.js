import React, { PureComponent } from 'react';
import styled from 'styled-components';

import {Counters} from '../Counters';
import {Counter} from '../Counter';
import {Post} from '../Post';
import {Slot} from '../Slot';
import {Cards} from '../Cards';

const Root = styled.div``;

const Header = styled.header`
    margin-bottom: 20px;
  
    @media (min-width: 660px) {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }
`;

const HeaderMain = styled.div`
    display: flex;
    align-items: baseline;
    margin-bottom: 10px;
    
    @media (min-width: 660px) {
        margin-bottom: 0;
    }
`;

const Title = styled.h2`
    margin: 0;
    font-size: 30px;
    font-weight: 700;
    margin-right: 20px;
`;

const Description = styled.span`
    font-size: 16px;
    color: #a6a6a6;
`;

export class Day extends PureComponent {
    static defaultProps = {
        cards: [],
        counters: [],
        onPostClick: () => {},
        onSlotClick: () => {},
        onSchedulePost: () => {}
    };

    renderCounter(counter, index) {
        return (
            <Counter
                key={index}
                type={counter.type}
                progress={counter.progress}
                count={counter.count}
            />
        );
    }

    renderCard = (onPostClick, onSlotClick) => (card, index) => {
        if (card.type === 'post') {
            return (
                <Post
                    key={index}
                    onClick={onPostClick}
                    triggerData={card}
                    time={card.time}
                    socials={card.socials}
                >
                    {card.content}
                </Post>
            );
        } else if (card.type === 'slot') {
            return (
                <Slot
                    key={index}
                    onClick={onSlotClick}
                    triggerData={card}
                    time={card.time}
                    text={card.text}
                />
            );
        }
    };

    render() {
        const {
            onSchedulePost,
            onPostClick,
            onSlotClick,
            title,
            description,
            counters,
            cards
        } = this.props;

        return (
            <Root>
                <Header>
                    <HeaderMain>
                        <Title>{title}</Title>
                        <Description>{description}</Description>
                    </HeaderMain>
                    <Counters>
                        {counters.map(this.renderCounter)}
                    </Counters>
                </Header>
                <Cards>
                    {cards.map(this.renderCard(onPostClick, onSlotClick))}
                    <Slot
                        triggerData='schedule'
                        onClick={onSchedulePost}
                    />
                </Cards>
            </Root>
        );
    }
}