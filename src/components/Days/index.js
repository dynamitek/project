import React, { PureComponent, Children } from 'react';
import styled from 'styled-components';

const Root = styled.ul`
    margin: 0;
    padding: 0;
    list-style: none;
`;

const Item = styled.li`
    &:not(:last-child) {
        margin-bottom: 35px;
    }
`;

export class Days extends PureComponent {
    renderItem(item, index) {
        return <Item key={index}>{item}</Item>;
    }

    render() {
        const {children} = this.props;

        return <Root>{Children.map(children, this.renderItem)}</Root>
    }
}