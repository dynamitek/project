import React, { PureComponent } from 'react';
import styled from 'styled-components';

const SPEED = '0.3s';
const TIMING = 'ease';

const Root = styled.button`
    position: relative;
    z-index: 0;
    display: block;
    min-width: 100%;
    padding: 0;
    margin: 0;
    border: none;
    outline: none;
    appearance: none;
    text-align: left;
    background: none;
    color: #2a2a2a;
    border-radius: 0;
    font-family: inherit;
    font-size: inherit;
    
    &::-moz-focus-inner {
        border: 0;
    }

    @media (min-width: 600px) {
        transition: z-index ${SPEED} ${TIMING};
        cursor: pointer;
    
        &:hover,
        &:focus {
            z-index: 1;
        
            &::before {
                background-color: ${({isNeutral}) => isNeutral ? '#f9f9f9' : '#fff'};
                transform: scale(1.05);
                box-shadow: 0 1px 20px rgba(0, 0, 0, 0.1);
            }
        }
        
        &:focus {
            outline: none;
        
            &::before {
                transform: scale(1.03);
                box-shadow: 0 1px 10px rgba(0, 161, 255, 0.5);
            }
        }
        
        &:active {
            &::before {
                transform: scale(0.95);
                box-shadow: none;
            }
        }
    }
    
    &::before {
        content: '';
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: ${({isNeutral}) => isNeutral ? 'transparent' : '#fff'};
        border-style: ${({isNeutral}) => isNeutral ? 'dashed' : 'solid'};
        border-width: 1px;
        border-color: #e3e3e3;
        
        @media (min-width: 600px) {
            transition: transform ${SPEED} ${TIMING},
                        box-shadow ${SPEED} ${TIMING},
                        background ${SPEED} ${TIMING};
        }
    }
`;

export class Card extends PureComponent {
    static defaultProps = {
        isNeutral: false,
        children: 'content',
        onClick: () => {}
    };

    handleClick = () => {
        const {onClick, triggerData} = this.props;

        onClick(triggerData);
    };

    render() {
        const {isNeutral, children} = this.props;

        return (
            <Root
                onClick={this.handleClick}
                isNeutral={isNeutral}
                type='button'
            >
                {children}
            </Root>
        );
    }
}