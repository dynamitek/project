import React, { PureComponent } from 'react';
import styled from 'styled-components';

import {Icon} from '../Icon';

const Header = styled.header`
    position: relative;
    min-height: 70px;
    padding: 25px 20px;
    padding-right: 80px;
    background-color: #fff;
    border-bottom: 1px solid #e2e2e2;
`;

const Title = styled.h3`
    margin: 0;
    font-size: 16px;
    font-weight: 700;
    color: #4a4a4a;
`;

const Close = styled.button`
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 68px;
    padding: 0;
    margin: 0;
    border: none;
    outline: none;
    appearance: none;
    background: none;
    border-radius: 0;
    color: #000;
    cursor: pointer;
    transition: transform 0.2s ease;
    
    &:active {
        transform: translateY(3px);
    }
`;

const Content = styled.div`
    padding: 20px;
    background-color: #f5f5f5;
`;

export class Popup extends PureComponent {
    static defaultProps = {
        onClose: () => {}
    };

    render() {
        const {
            title,
            onClose,
            children
        } = this.props;

        return (
            <div>
                <Header>
                    <Title>{title}</Title>
                    <Close type='button' onClick={onClose}>
                        <Icon type='cross' />
                    </Close>
                </Header>
                <Content>
                    {children}
                </Content>
            </div>
        );
    }
}