import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';

import {Popup} from '../Popup';

const Root = styled.div`
    position: fixed;
    z-index: 100;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    overflow: auto;  
`;

const Paranja = styled.div`
    position: absolute;
    z-index: 1;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.6);
`;

const Inner = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    min-height: 100%;
    
    @media (min-width: 700px) {
        align-items: flex-start;
    }
`;

const Content = styled.div`
    position: relative;
    z-index: 2;
    width: 100%;
    padding: 30px;

    @media (min-width: 700px) {
        width: 660px;
    }
`;

export class Modal extends PureComponent {
    static defaultProps = {
        onClose: () => {}
    };

    componentWillMount() {
        document.body.style.overflow = 'hidden';
        this.root = document.createElement('div');
        document.body.appendChild(this.root);
    }

    componentWillUnmount() {
        document.body.removeChild(this.root);
        document.body.removeAttribute('style');
    }

    render() {
        const {children, title, onClose} = this.props;

        return ReactDOM.createPortal(
            <Root>
                <Inner>
                    <Content>
                        <Popup title={title} onClose={onClose}>
                            {children}
                        </Popup>
                    </Content>
                    <Paranja onClick={onClose} />
                </Inner>
            </Root>,
            this.root
        );
    }
}