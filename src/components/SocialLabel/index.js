import React, { PureComponent } from 'react';
import styled from 'styled-components';

import {Icon} from '../Icon';

const colors = {
    facebook: '#3b5999',
    twitter: '#55acee',
    youtube: '#cd201f',
    googlePlus: '#cd201f',
    instagram: '#ff0084'
};

const Root = styled.span`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 10px;
    height: 10px;
    padding: 2px;
    background-color: ${({social}) => colors[social]};
`;

const StyledIcon = styled(Icon)`
    color: #fff;
`;

export class SocialLabel extends PureComponent {
    render() {
        const {social, className} = this.props;

        return (
            <Root social={social} className={className}>
                <StyledIcon type={social} responsive />
            </Root>
        );
    }
}