import React, { PureComponent } from 'react';
import styled from 'styled-components';

import {Card} from '../Card';
import {Icon} from '../Icon';
import {SocialLabel} from '../SocialLabel';

const Inner = styled.span`
    display: flex;
    flex-direction: column;
    height: 200px;
    padding: 20px;
    font-family: Stem, Arial, sans-serif;
`;

const Header = styled.span`
    display: flex;
    flex-shrink: 0;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 8px;
    line-height: 1;
`;

const Content = styled.span`
    position: relative;
    flex-grow: 1;
    overflow: hidden;
    font-size: 13px;
    line-height: 1.4;
    
    &::after {
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        height: 35px;
        background: linear-gradient(rgba(255, 255, 255, 0.001), white);
        pointer-events: none;
    }
`;

const Time = styled.span`
    font-weight: 700;
`;

const Socials = styled.span`
    display: flex;
    align-items: center;
`;

const Social = styled.span`
    &:not(:last-child) {
        margin-right: 5px;
    }
`;

const Account = styled.span`
    position: relative;
    display: block;
    width: 20px;
    height: 20px;
    background-size: cover;
    background-color: #e6e6e6;
    background-image: url(${({url}) => url});
`;

const AccountLabel = styled(SocialLabel)`
    position: absolute;
    bottom: 0;
    right: 0;
`;

export class Post extends PureComponent {
    static defaultProps = {
        onClick: () => {}
    };

    renderSocial({avatar, type}, index) {
        return (
            avatar ?
            <Social key={index}>
                <Account url={avatar}>
                    <AccountLabel social={type} />
                </Account>
            </Social>
            :
            <Social key={index}>
                <Icon type={type} />
            </Social>
        );
    }

    render() {
        const {
            onClick,
            triggerData,
            children,
            time,
            socials
        } = this.props;

        return (
            <Card
                onClick={onClick}
                triggerData={triggerData}
            >
                <Inner>
                    <Header>
                        <Time>{time}</Time>
                        <Socials>
                            {socials.map(this.renderSocial)}
                        </Socials>
                    </Header>
                    <Content>{children}</Content>
                </Inner>
            </Card>
        );
    }
}