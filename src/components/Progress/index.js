import React, { PureComponent } from 'react';
import styled from 'styled-components';

const Root = styled.svg`
    border-radius: 50%;
    background: #cdcccc; 
    transform: rotate(-90deg);
`;

const Circle = styled.circle`
    fill: none;
    stroke: ${({value}) => value > 0 ? '#2a2a2a' : '#cdcccc'};
    stroke-width: 32;
`;

export class Progress extends PureComponent {
    render() {
        const {value} = this.props;
        const dasharray = `${value} 100`;

        return (
            <Root
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 32 32'
            >
                <Circle
                    value={value}
                    strokeDasharray={dasharray}
                    r='16'
                    cx='16'
                    cy='16'
                />
            </Root>
        );
    }
}