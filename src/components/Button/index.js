import React, { PureComponent } from 'react';
import styled from 'styled-components';

const Inner = styled.span`
    display: block;
    transition: transform 0.2s ease;
`;

const Root = styled.button`
    display: inline-block;
    padding: 16px 20px;
    margin: 0;
    border: none;
    outline: none;
    appearance: none;
    background-color: #dcdcdc;
    color: #2a2a2a;
    font-weight: 700;
    text-transform: uppercase;
    border-radius: 3px;
    font-size: 13px;
    font-family: inherit;
    cursor: pointer;
    box-shadow: inset 0 -3px 0 0 rgba(0, 0, 0, 0.1);
    transition: transform 0.2s ease,
                box-shadow 0.2s ease,
                background-color 0.2s ease;
    
    &:focus {
        background-color: #ccc;
    }
    
    &:not(:disabled):active {
        box-shadow: none;
        background-color: #dcdcdc;
        
        ${Inner} {
            transform: translateY(3px);
        }
    }
    
    &:disabled {
        box-shadow: none;
        background-color: #eaeaea;
        color: #d5d5d5;
        cursor: auto;
    }
    
    &::-moz-focus-inner {
        border: 0;
    }
`;

export class Button extends PureComponent {
    static defaultProps = {
        onClick: () => {}
    };

    render() {
        const {
            onClick,
            children,
            disabled
        } = this.props;

        return (
            <Root
                type='button'
                disabled={disabled}
                onClick={onClick}
            >
                <Inner>
                    {children}
                </Inner>
            </Root>
        );
    }
}