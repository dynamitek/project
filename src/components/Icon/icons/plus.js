import React from 'react';

export const params = {
    'viewBox': '0 0 12 12',
    'width': 12,
    'height': 12
};

export const IconInner = () =>
    <path d='M5 5V0h2v5h5v2H7v5H5V7H0V5h5z' fillRule='evenodd'/>
;