import React from 'react';

export const params = {
    'viewBox': '0 0 12 11',
    'width': 12,
    'height': 11
};

export const IconInner = () =>
    <path
        d='M2.454 11h-.642l.378-.505c.286-.382.473-.869.569-1.483C.977 8.236 0 6.752 0 4.809 0 1.843 2.3 0 6 0s6 1.843 6 4.809c0 3.011-2.243 4.81-6 4.81-.084 0-.166-.003-.249-.006C5.188 10.274 4.177 11 2.454 11z'
        fillRule='evenodd'
    />;
