import React from 'react';

export const params = {
    'viewBox': '0 0 12 12',
    'width': 12,
    'height': 12
};

export const IconInner = () =>
    <g fillRule='nonzero'>
        <path d='M3.75 1.5A2.25 2.25 0 0 0 1.5 3.75v4.5a2.25 2.25 0 0 0 2.25 2.25h4.5a2.25 2.25 0 0 0 2.25-2.25v-4.5A2.25 2.25 0 0 0 8.25 1.5h-4.5zm0-1.5h4.5A3.75 3.75 0 0 1 12 3.75v4.5A3.75 3.75 0 0 1 8.25 12h-4.5A3.75 3.75 0 0 1 0 8.25v-4.5A3.75 3.75 0 0 1 3.75 0z'/>
        <path d='M6 4.5a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM6 3a3 3 0 1 1 0 6 3 3 0 0 1 0-6z'/>
        <rect x='8' y='2' width='2' height='2' rx='1'/>
    </g>;
