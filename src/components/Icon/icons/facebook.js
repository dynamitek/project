import React from 'react';

export const params = {
    'viewBox': '0 0 6 11',
    'width': 6,
    'height': 11
};

export const IconInner = () =>
    <path
        d='M3.833 3.833v-.992c0-.448.1-.674.795-.674H5.5V.5H4.167C2.5.5 1.833 1.612 1.833 2.833v1H.5V5.5h1.333v5h2v-5h1.469L5.5 3.833H3.833'
        fillRule='evenodd'
    />;