import React from 'react';

export const params = {
    'viewBox': '0 0 31.112 31.112',
    'width': 16,
    'height': 16
};

export const IconInner = () =>
    <path
        d='M31.112 1.414L29.698 0 15.556 14.142 1.414 0 0 1.414l14.142 14.142L0 29.698l1.414 1.414L15.556 16.97l14.142 14.142 1.414-1.414L16.97 15.556z'
    />;
