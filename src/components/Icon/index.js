import React, { PureComponent } from 'react';
import styled from 'styled-components';

import * as plus from './icons/plus'
import * as facebook from './icons/facebook'
import * as googlePlus from './icons/googlePlus'
import * as twitter from './icons/twitter'
import * as instagram from './icons/instagram'
import * as youtube from './icons/youtube'
import * as retweet from './icons/retweet'
import * as heart from './icons/heart'
import * as dialog from './icons/dialog'
import * as user from './icons/user'
import * as cross from './icons/cross'

const icons = {
    plus,
    facebook,
    googlePlus,
    twitter,
    instagram,
    youtube,
    retweet,
    heart,
    dialog,
    user,
    cross,
};

const Svg = styled.svg`
    display: block;
    fill: currentColor;
    width: ${({responsive}) => responsive ? '100%' : ''};
    height: ${({responsive}) => responsive ? '100%' : ''};
`;

export class Icon extends PureComponent {
    render() {
        const {
            viewBox,
            width,
            height,
            type,
            className,
            responsive
        } = this.props;

        const {
            params,
            IconInner
        } = icons[type];

        return (
            <Svg
                className={className}
                xmlns='http://www.w3.org/2000/svg'
                viewBox={viewBox || params.viewBox}
                width={width || params.width}
                height={height || params.height}
                responsive={responsive}
            >
                <IconInner />
            </Svg>
        );
    }
}