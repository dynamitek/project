import React, { Component } from 'react';
import styled from 'styled-components';

import {ActualDays} from '../../containers/ActualDays';

const Root = styled.div`
    padding: 30px;
    background-color: #f5f5f5;
`;

export class App extends Component {
    render() {
        return (
            <Root>
                <ActualDays />
            </Root>
        );
    }
}

