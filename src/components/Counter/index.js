import React, { PureComponent } from 'react';
import styled from 'styled-components';

import {Icon} from '../Icon';
import {Progress} from '../Progress';

const Root = styled.div`
    display: flex;
    align-items: center;
`;

const Status = styled.div`
    position: relative;
    width: 20px;
    height: 20px;
    margin-right: 5px;
`;

const StatusIcon = styled(Icon)`
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    top: 0;
    margin: auto;
    color: #fff;
`;

const Count = styled.span`
    color: #a6a6a6;
    font-size: 13px;
    line-height: 1;
`;

export class Counter extends PureComponent {
    render() {
        const {
            count,
            progress,
            type
        } = this.props;

        return (
            <Root>
                <Status>
                    <Progress value={progress} />
                    <StatusIcon type={type} />
                </Status>
                <Count>{count}</Count>
            </Root>
        );
    }
}