import React, { PureComponent } from 'react';
import styled from 'styled-components';

import {Card} from '../Card';
import {Icon} from '../Icon';

const Inner = styled.span`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 200px;
    padding: 36px;
`;

const Text = styled.span`
    font-size: 16px;
    text-align: center;
    margin-bottom: 8px;
`;

const Time = styled.span`
    font-weight: 700;
    font-size: 16px;
    line-height: 1;
    margin-bottom: 3px;
`;

export class Slot extends PureComponent {
    static defaultProps = {
        onClick: () => {},
        text: 'Schedule post on this day'
    };

    render() {
        const {
            onClick,
            triggerData,
            text,
            time
        } = this.props;

        return (
            <Card
                isNeutral
                onClick={onClick}
                triggerData={triggerData}
            >
                <Inner>
                    {time && <Time>{time}</Time>}
                    <Text>{text}</Text>
                    <Icon
                        type='plus'
                    />
                </Inner>
            </Card>
        );
    }
}