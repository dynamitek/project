import React, { PureComponent, Children } from 'react';
import styled from 'styled-components';

const Root = styled.ul`
    margin: 0;
    padding: 0;
    list-style: none;

    @media (min-width: 600px) {
        margin-left: 1px;
        margin-top: 1px;
        font-size: 0;
    }
`;

const Item = styled.li`
    & + & {
        margin-top: 10px;
        
        @media (min-width: 600px) {
            margin-top: -1px;
        }
    }
    
    @media (min-width: 600px) {
        display: inline-block;
        vertical-align: top;
        width: 200px;
        margin-top: -1px;
        margin-left: -1px;
        font-size: 13px;
    }
`;

export class Cards extends PureComponent {
    renderItem(item, index) {
        return <Item key={index}>{item}</Item>;
    }

    render() {
        const {children} = this.props;

        return <Root>{Children.map(children, this.renderItem)}</Root>
    }
}