import {combineReducers} from 'redux';
import accounts from './accounts';
import posts from './posts';
import stats from './stats';

const rootReducer = combineReducers({
    accounts,
    posts,
    stats
});

export default rootReducer;