import * as accountsSelectors from './accounts';

const posts = (state = [], action) => {
    return state;
};

export default posts;

export const getPost = (state, postId) => {
    const post = state.posts.find(post => post.id === postId);
    const id = postId;
    const type = 'post';
    const content = post.text;
    const socials = accountsSelectors.getPostAccounts(state, postId);
    const date = new Date(post.pubDate);
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const formattedHours = hours > 9 ? hours : `0${hours}`;
    const formattedMinutes = minutes > 9 ? minutes : `0${minutes}`;
    const time = `${formattedHours}:${formattedMinutes}`;

    return {
        id,
        type,
        content,
        socials,
        time
    };
};

export const getPostsInRange = (state, start, end) => {
    const posts = state.posts;
    const filteredPosts = posts.filter(post => post.pubDate >= start && post.pubDate <= end);

    return filteredPosts.map(post => getPost(state, post.id));
};