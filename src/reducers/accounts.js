import * as statsSelectors from './stats';

const accounts = (state = [], action) => {
    return state;
};

export default accounts;

const isMultiSocial = (accounts, socialName) => {
    return accounts.filter(account => account.social === socialName).length > 1;
};

export const getAccounts = state => state.accounts;

export const getStatsAccounts = (state, postStats) =>
    postStats.map(stat =>
        getAccounts(state).find(account =>
            account.id === stat.accountId)
    );

export const getPostAccounts = (state, postId) => {
    const postStats = statsSelectors.getPostStats(state, postId);
    const accounts = getStatsAccounts(state, postStats);

    return accounts.map(account => {
        const isMulti = isMultiSocial(getAccounts(state), account.social);

        if (isMulti) {
            return {
                type: account.social,
                avatar: account.avatar
            }
        }

        return {
            type: account.social
        }
    });
};
