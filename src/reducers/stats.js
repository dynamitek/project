import * as postsSelectors from './posts';

import {ONE_DAY_MS} from '../constants/time';

const stats = (state = [], action) => {
    return state;
};

export default stats;

const getStats = (state) => state.stats;

const sumStats = (stats) => {
    return stats.reduce((acc, stat) => {
        return {
            likes: acc.likes + stat.likes,
            retweets: acc.retweets + stat.retweets,
            comments: acc.comments + stat.comments,
            views: acc.views + stat.views
        };

    }, {
        likes: 0,
        retweets: 0,
        comments: 0,
        views: 0
    });
};

export const getPostStats = (state, postId) =>
    getStats(state).filter(stat => stat.postId === postId);

export const getDayCounters = (state, dayTimestamp) => {
    const posts = postsSelectors.getPostsInRange(state, dayTimestamp, dayTimestamp + ONE_DAY_MS);
    const stats = posts.map(post => getPostStats(state, post.id));

    const postCounters = stats.map(postStats => {
        return sumStats(postStats);
    });

    const counters = sumStats(postCounters);

    return [
        {
            type: 'user',
            progress: 0,
            count: counters.views
        },
        {
            type: 'retweet',
            progress: 0,
            count: counters.retweets
        },
        {
            type: 'heart',
            progress: 0,
            count: counters.likes
        },
        {
            type: 'dialog',
            progress: 0,
            count: counters.comments
        }
    ];
};
